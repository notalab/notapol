-- LOL-BASE INIT
print "lol-base init START"
-- initialize strict
require "libs.lol-base.strict.init"
print "=> lua strict initialized"
-- initailize debugging
global("lovebird")
lovebird = require("libs.lol-base.lovebird.init")
lovebird.port = 8000
print "=> lovebird initialized"
-- initialize gamera
global("UI_STATES")
UI_STATES = {
	pan = false,
}
global("css")
css = {
	screenSizeX = 1900,
	screenSizeY = 1000,
	graphRenderAreaTopLeftX = 0,
	--graphRenderAreaTopLeftY = 1000 * 0.35,
	graphRenderAreaTopLeftY = 0,
	--graphRenderAreaBottomRightX = 1900 * 0.6,
	graphRenderAreaBottomRightX = 1900,
	graphRenderAreaBottomRightY = 1000,
}
global("gamera")
gamera = require("client.libs.gamera.gamera")
global("camera")
camera = gamera.new(-5000,-5000,5000,5000)
camera:setWorld(
	css.graphRenderAreaTopLeftX-5000,
	css.graphRenderAreaTopLeftY-5000,
	10000,
	10000
)
camera:setWindow(
	css.graphRenderAreaTopLeftX,
	css.graphRenderAreaTopLeftY,
	css.graphRenderAreaBottomRightX,
	css.graphRenderAreaBottomRightY
)
camera:setPosition(
	(css.graphRenderAreaTopLeftX + css.graphRenderAreaBottomRightX) * 0.5,
	(css.graphRenderAreaTopLeftY + css.graphRenderAreaBottomRightY) * 0.5
)
print "=> gamera initialized"
-- initialize graphoon
global("graphoon")
graphoon = require('client.libs.graphoon.Graphoon').Graph
global("graph")
print "=> graphoon initialized"
print "lol-base init END"

-- GOOI INIT
require("client.libs.lol-gooi.init")

-- initailize input
global("fakeInput")
fakeInput = {
	"DUDE loves CHOCOLATE",
	"DUDE (is brother of) RUDA",
	"RUDA (is brother of) [DUDE]",
	"DUDE is SLIM",
	"DUDE is OLD",
	"DUDE (heard about) [DEATH OF RUDA]",
	"[DEATH OF RUDA] (is about) DEATH",
	"[DEATH OF RUDA] (is about) RUDA",
	"[DEATH OF RUDA] (happenned) [DAY AGO]",
	"BIRTH OF JURAJ happenned TWO DAYS AGO",
}
global("fakeInputCount")
fakeInputCount = #fakeInput

-- initailize internal representation
global("entities"); 
global("relationTypes");
global("nodes"); nodes = {}
global("metanodes"); metanodes = {}

global ("ProcessInputLines")
function ProcessInputLines()
	local function CountItems(someTable)
		local counter = 0
		for k,v in pairs(someTable) do
			counter = counter + 1
		end
		return counter
	end
	local oldNodeCount = CountItems(nodes)
	local oldMetanodeCount = CountItems(metanodes)
	
	entities = {}
	relationTypes = {}
	nodes = {}
	metanodes = {}
	
	for i=1, fakeInputCount do
		local line = fakeInput[i] or ""
		local occurences
		
		local lineArray = {}
		local nodeIndex = 1
		local relationIndex = 2
		local function IncreaseIndex(index) return index + 2 end
		
		-- all upper case multiple word items in brackets [] or without
		local multipleUpperCase = "([A-Z0-9][A-Z0-9%s]*[A-Z0-9])"
		for word in line:gmatch(multipleUpperCase) do
			entities[word] = true
			if nodes[word] == nil then
				nodes[word] = {
					inc = {},
					out = {}
				}
			end
			lineArray[nodeIndex] = word
			nodeIndex = IncreaseIndex(nodeIndex)
		end
		line, occurences = line:gsub(multipleUpperCase, "[.]") -- clean all occurences
		
		-- all lower case multiple word items in brackets () or without
		local multipleLowerCase = "([a-z0-9][a-z0-9%s]*[a-z0-9])"
		for word in line:gmatch(multipleLowerCase) do
			relationTypes[word] = true
			if metanodes[word] == nil then
				metanodes[word] = {
					inc = {},
					out = {}
				}
			end
			lineArray[relationIndex] = word
			relationIndex = IncreaseIndex(relationIndex)
		end
		line, occurences = line:gsub(multipleLowerCase, "[.]") -- clean all occurences
		
		-- graph structure
		if #lineArray > 2 then
			for i=1, #lineArray - 1, 2 do
				local firstNode = lineArray[i]
				local relationNode = lineArray[i+1]
				local secondNode = lineArray[i+2]
				local firstNodeNeighboursOutCount = #nodes[firstNode].out
				local secondNodeNeighboursIncCount = #nodes[secondNode].inc
				local relationNeigboursOutCount = #metanodes[relationNode].out
				local relationNeigboursIncCount = #metanodes[relationNode].inc
				nodes[firstNode].out[firstNodeNeighboursOutCount + 1] = secondNode
				nodes[secondNode].inc[secondNodeNeighboursIncCount + 1] = firstNode
				metanodes[relationNode].out[relationNeigboursOutCount + 1] = secondNode
				metanodes[relationNode].inc[relationNeigboursIncCount + 1] = firstNode
			end
		end
	end
	
	-- return there was a change
	return oldNodeCount ~= CountItems(nodes) or oldMetanodeCount ~= CountItems(metanodes)
end
ProcessInputLines()

global ("CreateGraph")
function CreateGraph()
	graph = graphoon.new()
	
	for k, v in pairs(nodes) do
		graph:addNode(k, love.graphics.getWidth(), love.graphics.getHeight())
	end
	for k, v in pairs(nodes) do
		if v.out ~= nil then
			for i=1, #v.out do
				graph:connectIDs(k, v.out[i])
			end
		end
	end
	graph:setAnchor(
		next(nodes), 
		(css.graphRenderAreaTopLeftX + css.graphRenderAreaBottomRightX) * 0.5, 
		(css.graphRenderAreaTopLeftY + css.graphRenderAreaBottomRightY) * 0.8
	)
	print "graph updated"
end

print "client initialized"
lovebird.update() -- send update to your browser localhost:<lovebird.port>

global("inputLines")
global("inputLinesCount")
inputLines = {}

global("InputLinesInit")
function InputLinesInit(oldCount, newCount)
	for i=1, oldCount do
		gooi.removeComponent(inputLines[i])
	end
	inputLines = {}
	for i=0, newCount-1 do
		inputLines[i+1] = gooi.newText(
			{
				y = 25 + i*25,
				x = 0,
				w = 500, 
				h = 20,
				bgColor = {0.208, 0.220, 0.222}
			}
		):setText(fakeInput[i+1] or "")
	end
end

-- load
function love.load()
	global("gr"); gr = love.graphics
	global("fontDir"); fontDir = "/fonts/"
	global("style"); style = {
		font = gr.newFont(fontDir.."Arimo-Bold.ttf", 13),
		showBorder = true,
		bgColor = {0.208, 0.220, 0.222}
	}
	gooi.setStyle(style)
	gooi.desktopMode()

	gr.setDefaultFilter("nearest", "nearest")
	
	inputLinesCount = gooi.newText(
		{
			y = 0,
			x = 300,
			w = 200, 
			h = 20,
			bgColor = {0.208, 0.220, 0.222}
		}
	):setText(tostring(fakeInputCount))
	
	-- create the input lines
	InputLinesInit(0,fakeInputCount)
	
	local success = love.window.setMode(1900, 1000)
end

function love.update(dt)
	local newInputCount = tonumber(inputLinesCount:getText())
	if 
		fakeInputCount ~= newInputCount and
		newInputCount ~= nil
	then
		-- re-create the input lines
		InputLinesInit(fakeInputCount, newInputCount)
		fakeInputCount = newInputCount
	end
	
	for i=1, fakeInputCount do
		if inputLines[i] then
			local text = inputLines[i]:getText()
			fakeInput[i] = text
		else
			fakeInput[i] = ""
		end
	end
	local inputUpdated = ProcessInputLines()
	if inputUpdated then
		CreateGraph()
	end
	
	-- GOOI update
	gooi.update(dt)
	
	-- camera update
	-- camera
	
	-- graph update
	if graph then
		graph:update(dt)
	end
end

global("DrawGraphScreen")
function DrawGraphScreen()
	if graph then
		graph:draw( function( node )
			local x, y = node:getPosition()
			love.graphics.circle( 'fill', x, y, 10 )
		end,
		function( edge )
			local ox, oy = edge.origin:getPosition()
			local tx, ty = edge.target:getPosition()
			love.graphics.line( ox, oy, tx, ty )
		end)
	end
end

-- draw loop
function love.draw()
	local screenWidth, screenHeight = love.graphics.getDimensions()
	
	local step = 15
	local titleOffsetY = step
	local titleOffsetX = step*2

	local inputX, inputY = 20, 20
	local listsTopY = 0
	local entitiesX, entitiesY = inputX + 25*titleOffsetX, listsTopY
	local relationsTypesColumn = inputX + 30*titleOffsetX
	local helloWorldAndStatsColumn = inputX + 35*titleOffsetX
	local nodesColumn = inputX + 40*titleOffsetX
	local metanodesColumn = inputX + 50*titleOffsetX
	
	local entitiesCount = 0
	local relationTypesCount = 0
	
	love.graphics.print("Input fields count: ", 0 , 0)
	love.graphics.print("Hello World!", helloWorldAndStatsColumn , entitiesY)
	love.graphics.print("New Input Count: " .. fakeInputCount, helloWorldAndStatsColumn , entitiesY + 2*titleOffsetY)
	
	love.graphics.print("Entities:", entitiesX, entitiesY)
	for k,_ in pairs(entities) do
		entitiesCount = entitiesCount + 1
		love.graphics.print(k, entitiesX, entitiesY + entitiesCount*step + 2*titleOffsetY)
	end
	love.graphics.print("Count:" .. entitiesCount, entitiesX, entitiesY + entitiesCount + titleOffsetY)
	
	local relationTypesY = entitiesY
	love.graphics.print("Relation types:", relationsTypesColumn, relationTypesY)
	for k,_ in pairs(relationTypes) do
		relationTypesCount = relationTypesCount + 1
		love.graphics.print(k, relationsTypesColumn, relationTypesY + relationTypesCount*step + 2*titleOffsetY)
	end
	love.graphics.print("Count:" .. relationTypesCount, relationsTypesColumn, relationTypesY + relationTypesCount + titleOffsetY)
	
	local function PrintNodeStructure(
		nameString,
		columnX,
		startY,
		nodeData
	)
		local nodesDataCount = 0
		
		love.graphics.print(nameString, columnX, startY)
		for k, data in pairs(nodeData) do
			nodesDataCount = nodesDataCount + 1
			love.graphics.print(k, columnX, startY + nodesDataCount*step + 2*titleOffsetY)
			if data.inc ~= nil then
				for i=1, #data.inc do
					nodesDataCount = nodesDataCount + 1
					love.graphics.print(">= " .. data.inc[i], columnX + titleOffsetX, startY + nodesDataCount*step + 2*titleOffsetY)
				end
			end
			if data.out ~= nil then
				for i=1, #data.out do
					nodesDataCount = nodesDataCount + 1
					love.graphics.print("=> " .. data.out[i], columnX + titleOffsetX, startY + nodesDataCount*step + 2*titleOffsetY)
				end
			end
		end
	end
	
	PrintNodeStructure("Nodes", nodesColumn, listsTopY, nodes)
	PrintNodeStructure("Metanodes", metanodesColumn, listsTopY, metanodes)

	-- draw GOOI elements
	gooi.draw()
	
	-- camera
	local function KKDH()
	end
	camera:draw(DrawGraphScreen)
	-- DrawGraphScreen()
	love.graphics.circle("fill", css.graphRenderAreaTopLeftX, css.graphRenderAreaTopLeftY, 7, 7)
	love.graphics.circle("fill", css.graphRenderAreaBottomRightX, css.graphRenderAreaBottomRightY, 7, 7)
	
	-- update all debug data
	lovebird.update() -- send update to your browser localhost:<lovebird.port>
end

function love.mousepressed(x, y, button) 
	-- GOOI event
	gooi.pressed()
	
	if button == 1 then -- left mouse button
		-- Click(x, y)
	end
	if button == 2 then -- right mouse button
		UI_STATES.pan = true
	end
end
function love.mousereleased(x, y, button)
	-- GOOI event
	gooi.released()
	
	-- other
	if button == 2 then -- right mouse button
		UI_STATES.pan = false
		print(camera:getPosition())
	end
end
function love.mousemoved(x, y, dx, dy, istouch)
	if UI_STATES.pan then
		local wx, wy = camera:getPosition()
		camera:setPosition(wx - dx, wy - dy)
	end
end
function love.textinput(text) gooi.textinput(text) end
function love.keyreleased(key, scancode) gooi.keyreleased(key, scancode) end
function love.keypressed(key, scancode, isrepeat)
  gooi.keypressed(key, scancode, isrepeat)
  if key == "escape" then
    quit()
  end
end